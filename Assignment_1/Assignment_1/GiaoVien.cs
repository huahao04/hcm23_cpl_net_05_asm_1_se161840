﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1
{
    public class GiaoVien : NguoiLaoDong 
    {
        public float HeSoLuong {  get; set; }
        
        public GiaoVien() { }
        public GiaoVien(string HoTen, int NamSinh, double LuongCoBan, float heSoLuong)
            : base(HoTen, NamSinh, LuongCoBan)
        {
           
           HeSoLuong = heSoLuong;
        }
        public void NhapThongTin(float heSoLuong)
        {
            HeSoLuong = heSoLuong;
 
        }
        public override double TinhLuong()
        {
            return LuongCoBan * HeSoLuong * 1.25;
        }
        public new void XuatThongTin()
        {
            base.XuatThongTin();
            Console.WriteLine("He so luong la: " + HeSoLuong);
            Console.WriteLine("Luong la: " + TinhLuong());

        }

    }
}
