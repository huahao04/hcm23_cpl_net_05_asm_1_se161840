﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1
{
    public class Validation
    {
        public static float inputFloat(string msg, float min, float max)
        {
        if (min > max) {
            float t = min;
            min = max;
            max = t;
        }
        float data;
        bool check = false;
        string input;
        do {
            
            Console.Write(msg);
            input = Console.ReadLine();
            check = float.TryParse(input, out data);
            if(!check)
            {
             Console.Write("Nhap sai du lieu, hay nhap lai: ");
            }
        } while (data < min || data > max || !check);
        return data;
        }
        public static double inputDouble(string msg, double min, double max)
        {
            if (min > max)
            {
                double t = min;
                min = max;
                max = t;
            }
            double data;
            bool check = false;
            do
            {
                string input;
                Console.Write(msg);
                input = Console.ReadLine();
                check = double.TryParse(input, out data);
                if (!check)
                {
                    Console.Write("Nhap sai du lieu, hay nhap lai: ");
                }

            } while (data < min || data > max || !check) ;
            return data;
        }
        public static int inputInt(string msg, int min, int max)
        {
            if (min > max)
            {
                int t = min;
                min = max;
                max = t;
            }
            int data;
            bool check = false;
            do
            {
                string input;
                Console.Write(msg);                
                input = Console.ReadLine();
                check = int.TryParse(input, out data);
                if (!check)
                {
                    Console.Write("Nhap sai du lieu, hay nhap lai: ");
                }
            } while (data < min || data > max || !check);
            return data;
        }
        public static string inputStr(string msg)
        {
            string data;
            do
            {
                Console.Write(msg);

                data = Console.ReadLine().Trim();

            } while (data.Length <= 0 );
            return data;

        }
    }
}
