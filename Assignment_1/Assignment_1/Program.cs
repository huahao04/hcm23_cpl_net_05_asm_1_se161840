﻿// See https://aka.ms/new-console-template for more information
using Assignment_1;

class Program
{
    static void Main()
    {
        
        int soLuong = Validation.inputInt("Nhap so luong giao vien: ", 1, 999);

        List<GiaoVien> danhSachGiaoVien = new List<GiaoVien>();

        for (int i = 1; i <= soLuong; i++)
        {
            Console.WriteLine("Nhap thong tin giao vien " + i + ":");

            
            string hoTen = Validation.inputStr("Nhap ten giao vien: ");

            
            int namSinh = Validation.inputInt("Nam sinh: ", 0, DateTime.Now.Year);
         
            
            double luongCoBan = Validation.inputDouble("Luong co ban: ", 0, 99999999999999999);

            
            float heSoLuong = Validation.inputFloat("He so luong: ", 0, 99999999999999999);

            GiaoVien giaoVien = new GiaoVien(hoTen, namSinh, luongCoBan, heSoLuong);
            danhSachGiaoVien.Add(giaoVien);
        }

        GiaoVien giaoVienLuongThapNhat = danhSachGiaoVien[0];
        double luongThapNhat = giaoVienLuongThapNhat.TinhLuong();

        foreach (GiaoVien gv in danhSachGiaoVien)
        {
            double luong = gv.TinhLuong();
            if (luong < luongThapNhat)
            {
                luongThapNhat = luong;
                giaoVienLuongThapNhat = gv;
            }
        }

        Console.WriteLine("Giao vien co luong thap nhat:");
        giaoVienLuongThapNhat.XuatThongTin();

        Console.WriteLine("Thoat........: ");
        Console.ReadKey();
    }
}